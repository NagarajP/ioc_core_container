package com.myzee.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.myzee.bean.AccountBean;

public class TestAccountBean {

	public static void main(String[] args) {

		/*
		 * For scope="prototype", for each request it creates one bean, whereas in scope="singleton" it creates 
		 * only one bean and for all the further requests it returns the same bean object
		 * scope = "singleton" in xml
		 */
		
		System.out.println("Using scope = \"singleton\" \n---------------------------------");
		// provide classpath for xml
		Resource r = new ClassPathResource("com/myzee/resources/spring.xml");

		// load xml file and beans
		BeanFactory factory = new XmlBeanFactory(r);

		AccountBean a = (AccountBean) factory.getBean("accountBean");
		AccountBean b = (AccountBean) factory.getBean("accountBean");
		AccountBean c = (AccountBean) factory.getBean("accountBean");
		b.checkBalance();

		/*------------------------------
		 * For scope="prototype", for each request it creates one bean, whereas in scope="singleton" it creates 
		 * only one bean and for all the further requests it returns the same bean object
		 * scope = "prototype" in xml
		 *------------------------------
		 */
		
		System.out.println("\n\nUsing scope = \"prototype\" \n---------------------------------");
		
		// provide classpath for xml
		Resource r_prototype = new ClassPathResource("com/myzee/resources/spring_prototype_scope.xml");

		// load xml file and beans
		BeanFactory factory_prototype = new XmlBeanFactory(r_prototype);

		AccountBean x = (AccountBean) factory_prototype.getBean("accountBean");
		AccountBean y = (AccountBean) factory_prototype.getBean("accountBean");
		AccountBean z = (AccountBean) factory_prototype.getBean("accountBean");
		x.checkBalance();

	}

}
